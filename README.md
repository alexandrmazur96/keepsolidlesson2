##Описание второго домашнего задания для интернатуры KeepSolid##
###Mazur Alexandr###
Перед работой с git'ом необходимо произвести его первоначальную настройку, как на локальной машине так и в удаленном репозиторие, в данном случае это **bitbucket**.    
Мои настройки:

1. Локальные настройки:
        
        ssh-keygen #установка ssh-ключа в системе        
        git config --global user.name "Alexandr Mazur"
        git config --global user.email alexandrmazur96@gmail.com   
   
2. Удаленные настройки:
   
        Установка public ssh-key в настройках аккаунта bitbucket        

После настройки git'a необходимо создать новый репозиторий на bitbucket'e.    

После создания нового репозитория следует клонировать этот репозиторий в свою рабочую папку (в данном случае, с помощью протокола ssh):
    
    git clone git@bitbucket.org:alexandrmazur96/keepsolidlesson2.git

Данная комманда склонирует указанный репозиторий в папку, в которой находится курсор рабочей папки в терминале. А так как данный репозиторий пока пустой, то и склонирует он пустой репозиторий, создав только папку с таким же названием, как и в репозитории.

Для работы с git'ом рекомендуется добавить файл `.gitignore`, он "говорит" *git'у* какие файлы следует игнорировать в командах *commit* & *push*

После этого был добавлен какой-то файл с кодом. Проверить изменения проекта можно коммандой:

    git status

Добавить файлы для фиксации (*commit'a*) можно коммандой:

    git add -A #префикс "-А" добавляет все измененные файлы
    #--all - аналог префикса -А

Файлы, добавленные для фиксации можно собственно зафиксировать:
   
    git commit -am "some text"
    
Данная комманда зафиксирует изменения на локальном компьютере, после чего можно будет "откатиться" в данную точку, в случае чего.
Для фиксации изменений на сервере следует "запушить" их туда, с помощью комманды:

    git push origin *branch_name*
    
Данная комманда загрузит все зафиксированные изменения на сервер bitbucket'a, после чего, с ними смогут работать другие члены команды, например с помощью команды:

    git fetch origin *branch_name*
    
можно получить "свежий" код из удаленного репозитория.
Для смены веток используется команда:

    git checkout *branch_name*

Для создания ветки можно воспользоваться предыдущей командой, с префиксом -b или -В:

    git checkout -b *branch_name*
    # || git checkout -B *branch_name* 

Если выбран префикс -В, данная команда проверяет, есть ли уже указанная ветка.

Для работы непосредственно с ветками используется команда:

    git branch
    
с помощью которой можно:

1. Получить список всех веток:
             
        git branch # || git branch --list    

2. Переименовать ветку:
    
        git branch -m *old_branch_name* *new_branch_name*
   
3. Удалить ветку:
     
        git branch -d *branch_name* 
   
4. Создать ветку
    
        git branch *new_branch_name*
   
        #Разница между 
   
        #git checkout -b *new_branch_name*
    
        #и данной командой в том, что данная команда не меняет текущую ветку, а просто создает новую.
   
Для просмотра истории комитов в текущей ветке используется команда:

    git log