<?php

/**
 * Class PullRequest_1 for pull request
 */
class PullRequest_1
{
    /**
     * function, that say 'hello'
     * @return string
     */
    public function Hello() : string{
        return "Hello!";
    }
}